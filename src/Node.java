/**
 * This class is the node class for a binary tree data structure
 * 
 * @author tyler_tracey
 * @version 1.1.1
 * 
 */
public class Node {
    /**
     * This Object is the Nodes current value
     */
    @SuppressWarnings("rawtypes")
    private Comparable current;
    /**
     * This Node is the left child node of the current node (pointer)
     */
    private Node leftChild;
    /**
     * This Node is the right child node of the current node (pointer)
     */
    private Node rightChild;

    /**
     * This is the constructor of Node
     * 
     * @param current
     *            Object that the node holds
     * @param left
     *            pointer to the left child Node
     * @param right
     *            pointer to the right child Node
     */
    @SuppressWarnings("rawtypes")
    public Node(Comparable current, Node left, Node right) {
        this.current = current;
        this.leftChild = left;
        this.rightChild = right;
    }

    /**
     * This methods sets the current Object
     * 
     * @param cur
     *            Object sets the current Node value
     */
    @SuppressWarnings("rawtypes")
    public void setCurrent(Comparable cur) {
        this.current = cur;
    }

    /**
     * returns the current Node value
     * 
     * @return current Object
     */
    @SuppressWarnings("rawtypes")
    public Comparable getCurrent() {
        return current;
    }

    /**
     * This method sets the left child value of the current Node
     * 
     * @param in
     *            Node that sets the left child pointer
     */
    public void setLeft(Node in) {
        this.leftChild = in;
    }

    /**
     * returns the leftChild Node of the current Node
     * 
     * @return leftChild Object
     */
    public Node getLeft() {
        return leftChild;
    }

    /**
     * This method sets the rightChild value of the current Node
     * 
     * @param in
     *            Node that sets the right child pointer
     */
    public void setRight(Node in) {
        this.rightChild = in;
    }

    /**
     * returns the rightChild Node of the current node
     * 
     * @return rightChild Object
     */
    public Node getRight() {
        return rightChild;
    }

    /**
     * Checks to see if the current node has no children Nodes
     * 
     * @return boolean whether or not the current Node is a leaf
     */
    public boolean isLeaf() {

        return (leftChild == null && rightChild == null);
    }

}