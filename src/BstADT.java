/**
 * This is the interface for BstADT
 * 
 * @author tyler_tracey
 * @version 1.1.1
 * 
 */
public interface BstADT {

    /**
     * This method takes in a value and returns whether or not that value exists
     * in the BST
     * 
     * @param val
     *            parameter to be searched for
     * @return boolean true if the value is found, false otherwise
     */
    @SuppressWarnings("rawtypes")
    public boolean contains(Comparable val);

    /**
     * This method inserts a value into the BST in the form of a Node
     * 
     * @param val
     *            to be inserted into the BST
     */
    @SuppressWarnings("rawtypes")
    public void insert(Comparable val);

    /**
     * @return boolean true if the BST is balanced, false otherwise
     */
    public boolean isBalanced();

    /**
     * This method returns whether or not the BST is empty
     * 
     * @return boolean true if empty, false otherwise
     */
    public boolean isEmpty();

    /**
     * this method returns a completely new BST of the left subtree of the
     * current BST
     * 
     * @return BstADT left subtree
     */
    public BstADT leftSubtree();

    /**
     * This method returns a string representation of the direction that the
     * program traverses the BST when looking for a specific value
     * 
     * @param val
     *            is the value to search for
     * @return String representation of the moves made towards the value,
     *         example ("LRRL")
     */
    @SuppressWarnings("rawtypes")
    public String moves(Comparable val);

    /**
     * This method preorder traverses the BST and returns the values in a string
     * format
     * 
     * @return String representation of the BST
     */
    public String preorder();

    /**
     * This method removes a value from the BST
     * 
     * @param val
     *            to be removed from the BST
     */
    @SuppressWarnings("rawtypes")
    public void remove(Comparable val);

    /**
     * This method returns a completely new BST of the right subtree of the
     * current BST
     * 
     * @return BstADT right subtree
     */
    public BstADT rightSubtree();

    /**
     * This method returns the value of the root of the current BST
     * 
     * @return Comparable value of the current root node
     */
    @SuppressWarnings("rawtypes")
    public java.lang.Comparable valOfRoot() throws IllegalStateException;

}
