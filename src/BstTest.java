import static org.junit.Assert.*;

import org.junit.Test;

/**
 * These are the JUnit test cases for BstADT
 * 
 * @author tyler_tracey
 * @version 1.1.1
 * 
 */
public class BstTest {

    /**
     * This tests the contains method when true
     */
    @Test
    public void testContainsTrue() {
        int a = 37;
        int b = 24;
        int c = 42;
        int d = 42;
        int e = 120;
        int f = 40;
        int g = 32;
        int h = 7;
        int i = 2;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        test.insert(e);
        test.insert(f);
        test.insert(g);
        test.insert(h);
        test.insert(i);
        assertEquals(true, test.contains(e));
    }

    /**
     * This tests the contains method when false
     */
    @Test
    public void testContainsFalse() {
        int a = 37;
        int b = 24;
        int c = 42;
        int d = 42;
        int e = 120;
        int f = 40;
        int g = 32;
        int h = 7;
        int i = 2;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        test.insert(e);
        test.insert(f);
        test.insert(g);
        test.insert(h);
        assertEquals(false, test.contains(i));
    }

    /**
     * This tests the contains method on an empty BST
     */
    @Test
    public void testContainsNullRoot() {
        Bst test = new Bst();
        assertEquals(false, test.contains(3));
    }

    /**
     * This tests the contains method on a Null Val
     */
    @Test
    public void testContainsNullVal() {
        Bst test = new Bst();
        test.insert(3);
        assertEquals(false, test.contains(null));
    }

    /**
     * This tests the contains method on an empty BST and null Val
     */
    @Test
    public void testContainsNullValAndNullRoot() {
        Bst test = new Bst();
        assertEquals(false, test.contains(null));
    }

    /**
     * This tests the contains method when value is in right subtree
     */
    @Test
    public void testContainsGoingRight() {
        Bst test = new Bst();
        test.insert(3);
        test.insert(4);
        test.insert(5);
        assertEquals(true, test.contains(5));
    }

    /**
     * This tests the insert method into an empty BST
     */
    @Test
    public void testInsertToNullBST() {
        int a = 1;
        BstADT test = new Bst();
        test.insert(a);
        assertEquals(1, test.valOfRoot());
    }

    /**
     * This tests the insert method into an empty BST
     */
    @Test
    public void testInsertANullVal() {
        int a = 1;
        BstADT test = new Bst();
        test.insert(a);
        test.insert(null);
        assertEquals(1, test.valOfRoot());
    }

    /**
     * This tests the insert method into a BST not empty
     */
    @Test
    public void testInsertMultipleRoot() {
        int a = 37;
        int b = 24;
        int c = 42;
        int d = 42;
        int e = 120;
        int f = 40;
        int g = 32;
        int h = 7;
        int i = 2;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        test.insert(e);
        test.insert(f);
        test.insert(g);
        test.insert(h);
        test.insert(i);
        assertEquals(37, test.valOfRoot());
    }

    /**
     * This test returns true if the BST is balanced
     */
    @Test
    public void testIsBalancedTrue() {
        int a = 37;
        int b = 24;
        int c = 42;
        int d = 42;
        int e = 120;
        int f = 40;
        int g = 32;
        int h = 7;
        int i = 2;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        test.insert(e);
        test.insert(f);
        test.insert(g);
        test.insert(h);
        test.insert(i);
        assertEquals(true, test.isBalanced());
    }

    /**
     * This test returns true if the BST is balanced
     */
    @Test
    public void testIsBalancedTrueWithNullLeftTree() {
        int h = 2;
        int i = 3;
        Bst test = new Bst();
        test.insert(h);
        test.insert(i);
        assertEquals(true, test.isBalanced());
    }

    /**
     * This test returns false if the BST if not balanced
     */
    @Test
    public void testIsBalancedFalse() {
        int a = 4;
        int b = 5;
        int c = 6;
        int d = 7;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        assertEquals(false, test.isBalanced());
    }

    /**
     * This test returns true if the empty BST is balanced
     */
    @Test
    public void testIsBalancedTrueEmptyBST() {
        Bst test = new Bst();
        assertEquals(true, test.isBalanced());
    }

    /**
     * This tests the isEmpty method is true
     */
    @Test
    public void testIsEmptyTrue() {
        Bst test = new Bst();
        assertEquals(true, test.isEmpty());
    }

    /**
     * This tests the isEmpty method is false
     */
    @Test
    public void testIsEmptyFalse() {
        Bst test = new Bst();
        test.insert(3);
        assertEquals(false, test.isEmpty());
    }

    /**
     * This method tests that the left subtree is equal
     */
    @Test
    public void testLeftSubtreeEqual() {
        int a = 3;
        int b = 2;
        int c = 1;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        Bst left = new Bst();
        left.insert(b);
        left.insert(c);
        assertEquals(left.preorder(), test.leftSubtree().preorder());
    }

    /**
     * This method tests that the left subtree is not equal
     */
    @Test(expected = IllegalStateException.class)
    public void testLeftSubtreeNull() {
        int a = 4;
        int b = 5;
        int c = 6;
        int d = 7;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        assertNotNull(test.leftSubtree().valOfRoot());
    }

    /**
     * This tests the moves method going to the left subtree
     */
    @Test
    public void testMovesLeft() {
        int a = 37;
        int b = 24;
        int c = 42;
        int d = 42;
        int e = 120;
        int f = 40;
        int g = 32;
        int h = 7;
        int i = 2;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        test.insert(e);
        test.insert(f);
        test.insert(g);
        test.insert(h);
        test.insert(i);
        assertEquals("LR", test.moves(32));
    }

    /**
     * This tests the moves method going to the right subtree
     */
    @Test
    public void testMovesRight() {
        int a = 37;
        int b = 24;
        int c = 42;
        int d = 42;
        int e = 120;
        int f = 40;
        int g = 32;
        int h = 7;
        int i = 2;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        test.insert(e);
        test.insert(f);
        test.insert(g);
        test.insert(h);
        test.insert(i);
        assertEquals("RLL", test.moves(40));
    }

    /**
     * This tests the moves method when the value is not in the BST
     */
    @Test
    public void testMovesWhenValueIsntInBST() {
        int a = 37;
        int b = 24;
        int c = 42;
        int d = 42;
        int e = 120;
        int f = 40;
        int g = 32;
        int h = 7;
        int i = 2;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        test.insert(e);
        test.insert(f);
        test.insert(g);
        test.insert(h);
        test.insert(i);
        assertEquals("RRR", test.moves(121));
    }

    /**
     * This tests the moves method when the value is not in the BST
     */
    @Test
    public void testMovesWhenValueIsNUll() {

        Bst test = new Bst();
        test.insert(5);
        assertEquals("", test.moves(null));
    }

    /**
     * This tests that the preorder traversal returns node values in the correct
     * order
     */
    @Test
    public void testPreorder() {
        int a = 37;
        int b = 24;
        int c = 42;
        int d = 42;
        int e = 120;
        int f = 40;
        int g = 32;
        int h = 7;
        int i = 2;
        int j = 30;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        test.insert(e);
        test.insert(f);
        test.insert(g);
        test.insert(h);
        test.insert(i);
        test.insert(j);
        assertEquals("< 37 24 7 2 32 30 42 42 40 120 >", test.preorder());
    }

    /**
     * This tests the preorder traversal method on an empty BST
     */
    @Test
    public void testPreorderEmptyBST() {
        Bst test = new Bst();
        assertEquals("<  >", test.preorder());
    }

    /**
     * This tests the removal of a leaf node
     */
    @Test
    public void testRemoveLeaf() {
        int a = 37;
        int b = 24;
        int c = 42;
        int d = 42;
        int e = 120;
        int f = 40;
        int g = 32;
        int h = 7;
        int i = 2;
        int j = 30;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        test.insert(e);
        test.insert(f);
        test.insert(g);
        test.insert(h);
        test.insert(i);
        test.insert(j);
        test.remove(30);
        assertEquals("< 37 24 7 2 32 42 42 40 120 >", test.preorder());

    }

    /**
     * This tests the removal of a root node
     */
    @Test
    public void testRemoveRoot() {
        int a = 37;
        int b = 24;
        int c = 42;
        int d = 42;
        int e = 120;
        int f = 40;
        int g = 32;
        int h = 7;
        int i = 2;
        int j = 30;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        test.insert(e);
        test.insert(f);
        test.insert(g);
        test.insert(h);
        test.insert(i);
        test.insert(j);
        test.remove(37);
        assertEquals("< 32 24 7 2 30 42 42 40 120 >", test.preorder());
    }

    /**
     * This tests the remove method when root is null
     */
    @Test
    public void testRemoveRootNull() {
        Bst test = new Bst();
        test.remove(37);
        assertEquals("<  >", test.preorder());
    }

    /**
     * This tests the remove method when value is null
     */
    @Test
    public void testRemoveValueNull() {
        Bst test = new Bst();
        test.insert(37);
        test.remove(null);
        assertEquals("< 37 >", test.preorder());
    }

    /**
     * This tests the remove method when root is null and value null
     */
    @Test
    public void testRemoveRootNullAndValueNull() {
        Bst test = new Bst();
        test.remove(null);
        assertEquals("<  >", test.preorder());
    }

    /**
     * This tests the removal of an internal node
     */
    @Test
    public void testRemoveInternalNode() {
        int a = 37;
        int b = 24;
        int c = 42;
        int d = 42;
        int e = 120;
        int f = 40;
        int g = 32;
        int h = 7;
        int i = 2;
        int j = 30;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        test.insert(d);
        test.insert(e);
        test.insert(f);
        test.insert(g);
        test.insert(h);
        test.insert(i);
        test.insert(j);
        test.remove(32);
        assertEquals("< 37 24 7 2 30 42 42 40 120 >", test.preorder());
    }

    /**
     * This tests the right subtree is equal
     */
    @Test
    public void testRightSubtreeEqual() {
        int a = 1;
        int b = 2;
        int c = 3;
        Bst test = new Bst();
        test.insert(a);
        test.insert(b);
        test.insert(c);
        Bst right = new Bst();
        right.insert(b);
        right.insert(c);
        assertEquals(right.preorder(), test.rightSubtree().preorder());
    }

    /**
     * This tests the right subtree is unequal
     */
    @Test(expected = IllegalStateException.class)
    public void testRightSubtreeNull() {
        int a = 4;
        int b = 5;
        int c = 6;
        int d = 7;
        Bst test = new Bst();
        test.insert(d);
        test.insert(c);
        test.insert(b);
        test.insert(a);
        assertNotNull(test.rightSubtree().valOfRoot());
    }

    /**
     * This tests the valOfRoot method on a null BST
     */
    @Test(expected = IllegalStateException.class)
    public void testValOfRootFalse() {
        Bst test = new Bst();
        assertNotNull(test.valOfRoot());
    }

}
