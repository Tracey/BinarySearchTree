/**
 * This class implements a binary search tree data structure with methods to
 * traverse the BST
 * 
 * @author traceys5
 * @version 1.1.1
 *
 */
public class Bst implements BstADT {

    /**
     * This field is the node root of the current BST
     */
    private Node root;

    /**
     * This is the string of preorder when that method is called
     */
    private String stringPreorder;

    /**
     * This is the default constructor for BST
     */
    public Bst() {
        this.root = null;
    }

    /**
     * This secondary constructor is used to return a subtree
     * 
     * @param in
     *            Node that is set as the root
     */
    private Bst(Node in) {
        this.root = in;
    }

    /**
     * This method takes in a value and returns whether or not that value exists
     * in the BST
     * 
     * @param val
     *            parameter to be searched for
     * @return boolean true if the value is found, false otherwise
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public boolean contains(Comparable val) {
        Node tmpRoot = root;
        if (val == null || tmpRoot == null) {
            return false;
        }
        while (tmpRoot.getCurrent().compareTo(val) != 0) {

            if (tmpRoot.isLeaf()) {
                return false;
            }
            else if (tmpRoot.getCurrent().compareTo(val) > 0) {
                tmpRoot = tmpRoot.getLeft();
            }
            else if (tmpRoot.getCurrent().compareTo(val) < 0) {
                tmpRoot = tmpRoot.getRight();
            }
        }
        return true;
    }

    /**
     * This method inserts a value into the BST in the form of a Node
     * 
     * @param val
     *            to be inserted into the BST
     */
    @SuppressWarnings({ "rawtypes" })
    public void insert(Comparable val) {
        if (val == null) {
            return;
        }
        this.root = insertHelp(root, val);
    }

    /**
     * @return boolean true if the BST is balanced, false otherwise
     */
    @Override
    public boolean isBalanced() {
        if (root == null) {
            return true;
        }
        int leftHeight = height(root.getLeft());
        int rightHeight = height(root.getRight());
        int diff = Math.abs(leftHeight - rightHeight);
        return (diff <= 1);
    }

    /**
     * This helper method returns the height of a BST
     * 
     * @param in
     *            Node as the root of a subtree
     * @return value integer of the height
     */
    private int height(Node in) {
        if (in == null) {
            return 0;
        }
        return (Math.max(height(in.getLeft()), height(in.getRight()))) + 1;
    }

    /**
     * This method returns whether or not the BST is empty
     * 
     * @return boolean true if empty, false otherwise
     */
    public boolean isEmpty() {
        return (root == null);
    }

    /**
     * this method returns a completely new BST of the left subtree of the
     * current BST
     * 
     * @return BstADT left subtree
     */
    public BstADT leftSubtree() {
        if (root.getLeft() == null) {
            Bst left = new Bst();
            return left;
        }
        Bst left = new Bst(root.getLeft());
        return left;
    }

    /**
     * This method returns a string representation of the direction that the
     * program traverses the BST when looking for a specific value
     * 
     * @param val
     *            is the value to search for
     * @return String representation of the moves made towards the value,
     *         example ("LRRL")
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public String moves(Comparable val) {
        String tmp = "";
        Node tmpRoot = root;
        if (val == null) {
            return tmp;
        }
        while (tmpRoot.getCurrent().compareTo(val) != 0
                && tmpRoot.getCurrent() != null) {

            if (tmpRoot.getCurrent().compareTo(val) > 0) {
                tmp += "L";
                if (tmpRoot.getLeft() != null) {
                    tmpRoot = tmpRoot.getLeft();
                }
                else {
                    return tmp;
                }
            }
            else if (tmpRoot.getCurrent().compareTo(val) < 0) {
                tmp += "R";
                if (tmpRoot.getRight() != null) {
                    tmpRoot = tmpRoot.getRight();
                }
                else {
                    return tmp;
                }
            }
        }
        return tmp;
    }

    /**
     * This method preorder traverses the BST and returns the values in a string
     * format
     * 
     * @return String representation of the BST
     */
    @Override
    public String preorder() {
        stringPreorder = "< ";
        if (root == null) {
            return "<  >";
        }
        preorderHelp(root);
        stringPreorder += ">";
        return stringPreorder;
    }

    /**
     * This helper method prints a preorder traversal of the BST to the private
     * field stringPreorder
     * 
     * @param in
     *            is the Node to be recursively called
     */
    private void preorderHelp(Node in) {
        if (in != null) {
            stringPreorder += in.getCurrent() + " ";
            preorderHelp(in.getLeft());
            preorderHelp(in.getRight());
        }
    }

    /**
     * This method removes a value from the BST
     * 
     * @param val
     *            to be removed from the BST
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void remove(Comparable val) {
        if (val == null || root == null) {
            return;
        }
        if (root.getCurrent().compareTo(val) == 0 && root.isLeaf()) {
            this.root = null;
        }
        else {
            removeHelp(root, val);
        }
    }

    /**
     * This helper method is used in unison with the remove method by
     * recursively finding the value to be removed, and modifying the Nodes to
     * their correct location once the node has been removed
     * 
     * @param parent
     *            is the root node of the BST
     * @param val
     *            is the value to be searched for and removed from the BST
     * @return Node that is the root of the subtree after modifying the BST post
     *         removal of the value
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Node removeHelp(Node parent, Comparable val) {

        // if the value is in the left subtree
        if (parent.getCurrent().compareTo(val) > 0) {
            parent.setLeft(removeHelp(parent.getLeft(), val));
        }
        // if the value is in the right subtree
        else if (parent.getCurrent().compareTo(val) < 0) {
            parent.setRight(removeHelp(parent.getRight(), val));
        }
        // found the value
        else {

            // return the right child
            if (parent.getLeft() == null) {
                return parent.getRight();
            }
            // return the left child
            else if (parent.getRight() == null) {
                return parent.getLeft();
            }
            // the value has 2 children
            else {
                Node tmp = maxNode(parent.getLeft());
                parent.setCurrent(tmp.getCurrent());
                parent.setLeft(deleteMax(parent.getLeft()));
            }

        }
        return parent;
    }

    /**
     * This helper method is used when the node to be removed from the BST has 2
     * children
     * 
     * @param parent
     *            is the leftChild node of the Node to be removed
     * @return Node that is the maximum value of the left subtree of the Node to
     *         be removed
     */
    private Node maxNode(Node parent) {
        while (parent.getRight() != null) {
            parent = parent.getRight();
        }
        return parent;
    }

    /**
     * This method finds the node that replaced the original parent node and
     * removes it from the left subtree
     * 
     * @param parent
     *            is the root of the left subtree that was removed
     * @return Node that the replaced node will point to, after it has been
     *         modified
     */
    private Node deleteMax(Node parent) {
        Node max = maxNode(parent);
        if (parent == maxNode(parent)) {
            return null;
        }

        while ((parent.getRight() != max)) {
            parent = parent.getRight();
        }
        if (!(max.isLeaf())) {
            parent.setRight(max.getLeft());
            return parent;
        }
        return parent;
    }

    /**
     * This method is used in unison with the insert method by recursively
     * adding an input value to its rightful place in the BST
     * 
     * @param parent
     *            is the root Node of the BST
     * @param val
     *            is the value to be added to the BST
     * @return Node root of the BST after adding the value
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Node insertHelp(Node parent, Comparable val) {
        if (parent == null) {
            return new Node(val, null, null);
        }
        else if (parent.getCurrent().compareTo(val) >= 0) {
            parent.setLeft(insertHelp(parent.getLeft(), val));
        }
        else {
            parent.setRight(insertHelp(parent.getRight(), val));
        }
        return parent;
    }

    /**
     * This method returns a completely new BST of the right subtree of the
     * current BST
     * 
     * @return BstADT right subtree
     */
    public BstADT rightSubtree() {
        if (root.getRight() == null) {
            Bst right = new Bst();
            return right;
        }
        Bst right = new Bst(root.getRight());
        return right;
    }

    /**
     * This method returns the value of the root of the current BST
     * 
     * @return Comparable value of the current root node
     */
    @SuppressWarnings({ "rawtypes" })
    public java.lang.Comparable valOfRoot() throws IllegalStateException {
        if (root != null) {
            return root.getCurrent();
        }
        else {
            throw new java.lang.IllegalStateException();

        }
    }

}